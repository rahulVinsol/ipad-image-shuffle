//
//  CollectionViewCell.swift
//  ExerciseImageShuffle
//
//  Created by Rahul Rawat on 02/07/21.
//

import UIKit
import Photos

class CollectionViewCell: UICollectionViewCell {
    static let IDENTIFIER = "cell"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    func setupView(with asset: Asset, size: CGSize) {
        title.text = asset.title
        PHImageManager.default().requestImage(for: asset.asset, targetSize: size, contentMode: .aspectFit, options: nil) { [weak self] (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            guard let self = self, let image = image else { return }
            
            self.imageView.image = image
        }
    }
}

