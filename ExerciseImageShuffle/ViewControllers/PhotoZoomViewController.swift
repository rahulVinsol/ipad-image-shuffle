//
//  PhotoZoomViewController.swift
//  ExerciseImageShuffle
//
//  Created by Rahul Rawat on 02/07/21.
///
//  PhotoZoomViewController.swift

import UIKit
import Photos

protocol PhotoZoomViewControllerDelegate {
    func imageLoaded(image: UIImage, forIndex index: Int)
    func didZoomIn()
    func didZoomOut()
}

class PhotoZoomViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTrailingConstraint: NSLayoutConstraint!
    
    var delegate: PhotoZoomViewControllerDelegate?
    
    var image: UIImage?
    var asset: PHAsset!
    var index: Int!
    
    var doubleTapGestureRecognizer: UITapGestureRecognizer!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapWith))
        self.doubleTapGestureRecognizer.numberOfTapsRequired = 2
    }
    
    func setParams(index: Int, asset: PHAsset, image: UIImage?) {
        self.index = index
        self.asset = asset
        self.image = image
        
        if image == nil {
            let requestOptions = PHImageRequestOptions()
            requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
            requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
            requestOptions.isSynchronous = false
            
            if (asset.mediaType == PHAssetMediaType.image) {
                PHImageManager.default().requestImage(for: asset, targetSize: view.frame.size, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { [weak self] (pickedImage, info) in
                    guard let self = self, let pickedImage = pickedImage else { return }
                    
                    self.delegate?.imageLoaded(image: pickedImage, forIndex: self.index)
                    self.imageView?.image = pickedImage
                })
            }
        }
    }
    
    @objc func didDoubleTapWith(_ gestureRecognizer: UITapGestureRecognizer) {
        let pointInView = gestureRecognizer.location(in: self.imageView)
        var newZoomScale = self.scrollView.maximumZoomScale
        
        if self.scrollView.zoomScale >= newZoomScale || abs(self.scrollView.zoomScale - newZoomScale) <= 0.01 {
            newZoomScale = self.scrollView.minimumZoomScale
        }
        
        let width = self.scrollView.bounds.width / newZoomScale
        let height = self.scrollView.bounds.height / newZoomScale
        let originX = pointInView.x - (width / 2.0)
        let originY = pointInView.y - (height / 2.0)
        
        let rectToZoomTo = CGRect(x: originX, y: originY, width: width, height: height)
        self.scrollView.zoom(to: rectToZoomTo, animated: true)
        
        if newZoomScale > self.scrollView.minimumZoomScale {
            delegate?.didZoomIn()
        } else {
            delegate?.didZoomOut()
        }
    }
    
    func updateConstraint() {
        let hasZoomed = scrollView.zoomScale > scrollView.minimumZoomScale
        
        let inset = view.safeAreaInsets
        let navHeight = navigationController?.navigationBar.frame.height ?? 0
        var statusBarHeight = view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        statusBarHeight = statusBarHeight > 0 ? statusBarHeight : inset.top
        
        self.imageViewBottomConstraint.constant = hasZoomed ? 0 : inset.bottom
        self.imageViewLeadingConstraint.constant = inset.left
        self.imageViewTopConstraint.constant = hasZoomed ? 0 : (navHeight + statusBarHeight)
        self.imageViewTrailingConstraint.constant = inset.right
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        self.view.addGestureRecognizer(self.doubleTapGestureRecognizer)
        
        scrollView.zoomScale = 1
        scrollView.maximumZoomScale = 4
        
        if let image = image {
            imageView.image = image
        }
        
        updateConstraint()
    }
    
    override func viewDidLayoutSubviews() {
        updateConstraint()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.scrollView.setZoomScale(1, animated: false)
    }
}

extension PhotoZoomViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateConstraint()
        if scrollView.zoomScale > 1 {
            if let imageView = imageView {
                
                let inset = view.safeAreaInsets
                var statusBarHeight = view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
                statusBarHeight = statusBarHeight > 0 ? statusBarHeight : inset.top
                
                let contentRect = imageView.contentClippingRect.multiply(n: scrollView.zoomScale)
                
                let top = 0.5 * (UIScreen.main.bounds.height * 4 - contentRect.size.height)
                let left = 0.5 * (UIScreen.main.bounds.width * 4 - contentRect.size.width)
                
                scrollView.contentInset = UIEdgeInsets(top: -top - statusBarHeight, left: left, bottom: -top - inset.bottom, right: left)
            }
        } else {
            scrollView.contentInset = .zero
        }
    }
}

extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat = min(bounds.width / image.size.width, bounds.height / image.size.height)
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}

extension CGRect {
    func multiply(n: CGFloat) -> CGRect {
        return CGRect(x: n * origin.x, y: n * origin.y, width: n * size.width, height: n * size.height)
    }
}
