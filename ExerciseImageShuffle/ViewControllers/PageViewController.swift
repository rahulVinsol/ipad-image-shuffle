//
//  PageViewController.swift
//  ExerciseImageShuffle
//
//  Created by Rahul Rawat on 02/07/21.
//

import UIKit
import Photos

class PageViewController: BasePageViewController {
    var galleryImages: [Asset]!
    var currentIndex: Int!
    
    private var nextIndex: Int?
    private var imageCache: NSCache<NSString, UIImage> = NSCache()
    private var shareImageButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .systemBackground
        
        let initialVC = getViewController(forIndex: currentIndex, with: galleryImages[currentIndex])
        
        self.dataSource = self
        self.delegate = self
        
        setViewControllers([initialVC], direction: .forward, animated: true, completion: nil)
        
        shareImageButton = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up"), style: .plain, target: self, action: #selector(shareImage))
        
        navigationItem.rightBarButtonItem = shareImageButton
    }
    
    @objc private func shareImage(_ sender: UIButton) {
        if let index = currentIndex, let image = imageCache.object(forKey: NSString(string: "\(index)")) {
            let activityViewController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            activityViewController.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
            activityViewController.excludedActivityTypes = [
                UIActivity.ActivityType.airDrop,
                UIActivity.ActivityType.message,
                UIActivity.ActivityType.print,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.mail,
                UIActivity.ActivityType.assignToContact
            ]
            
            present(activityViewController, animated: true, completion: nil)
        }
    }
    
    private func getViewController(forIndex index: Int, with asset: Asset) -> PhotoZoomViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(PhotoZoomViewController.self)") as! PhotoZoomViewController
        
        vc.setParams(index: index, asset: asset.asset, image: imageCache.object(forKey: NSString(string: "\(index)")))
        vc.delegate = self
        
        self.singleTapGestureRecognizer.require(toFail: vc.doubleTapGestureRecognizer)
        
        return vc
    }
}

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let prevIndex: Int
        
        if currentIndex == 0 {
            prevIndex = galleryImages.count - 1
        } else {
            prevIndex = currentIndex - 1
        }
        
        return getViewController(forIndex: prevIndex, with: galleryImages[prevIndex])
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let nextIndex: Int
        
        if currentIndex == galleryImages.count - 1 {
            nextIndex = 0
        } else {
            nextIndex = currentIndex + 1
        }
        
        return getViewController(forIndex: nextIndex, with: galleryImages[nextIndex])
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        guard let nextVC = pendingViewControllers.first as? PhotoZoomViewController else {
            return
        }
        
        self.nextIndex = nextVC.index
    }
}

extension PageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (completed && self.nextIndex != nil) {
            self.currentIndex = nextIndex!
        }
        
        self.nextIndex = nil
    }
}

extension PageViewController: PhotoZoomViewControllerDelegate {
    func didZoomIn() {
        changeScreenMode(to: .full)
    }
    
    func didZoomOut() {
        changeScreenMode(to: .normal)
    }
    
    func imageLoaded(image: UIImage, forIndex index: Int) {
        imageCache.setObject(image, forKey: NSString(string: "\(index)"))
    }
}
