//
//  ViewController.swift
//  ExerciseImageShuffle
//
//  Created by Rahul Rawat on 02/07/21.
//

import UIKit
import Photos

enum Section {
  case main
}

typealias DataSource = UICollectionViewDiffableDataSource<Section, Asset>
typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Asset>

class ViewController: UIViewController {
    private let itemsPerRow: CGFloat = 3
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    private lazy var widthPerItem: CGFloat = {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        return availableWidth / itemsPerRow
    }()
    private weak var customGalleryLayout: CustomGalleryLayout?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var galleryImages: [Asset]?
    private var selectedIndex: Int?
    
    private var shuffleButton: UIBarButtonItem!
    
    private lazy var dataSource = makeDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        
        if #available(iOS 14, *) {
            PHPhotoLibrary.requestAuthorization(for: .readWrite) { [weak self] (status) in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.showUI(for: status)
                }
            }
        } else {
            PHPhotoLibrary.requestAuthorization { [weak self] status in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.showUI(for: status)
                }
            }
        }
        
        PHPhotoLibrary.shared().register(self)
        
        if let layout = collectionView?.collectionViewLayout as? CustomGalleryLayout {
            customGalleryLayout = layout
            layout.delegate = self
        }
        
        shuffleButton = UIBarButtonItem(image: UIImage(systemName: "shuffle"), style: .plain, target: self, action: #selector(shuffleSelected))
        
        navigationItem.rightBarButtonItem = shuffleButton
    }
    
    @objc private func shuffleSelected(_ sender: UIButton) {
        shuffleImages()
    }
    
    private func shuffleImages() {
        galleryImages?.shuffle()
        
        applySnapshot(animatingDifferences: true)
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            shuffleImages()
        }
    }
    
    func applySnapshot(animatingDifferences: Bool = true) {
        guard let galleryImages = galleryImages else { return }
        
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(galleryImages)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    private func showUI(for status: PHAuthorizationStatus) {
        switch status {
        case .authorized, .limited:
            showPhotos()
            
        case .restricted:
            
            break
            
        case .denied:
            showAccessDeniedUI()
            
        case .notDetermined:
            break
            
        default:
            break
        }
    }
    
    private func showRestrictedUI() {
        let alert = UIAlertController(title: "Restricted", message: "Phone in Restricted mode can't do anything.", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func showAccessDeniedUI() {
        let alert = UIAlertController(title: "Allow access to your photos", message: "Go to your settings and tap \"Photos\".", preferredStyle: .alert)
        
        let notNowAction = UIAlertAction(title: "Not Now", style: .cancel, handler: nil)
        alert.addAction(notNowAction)
        
        let openSettingsAction = UIAlertAction(title: "Open Settings", style: .default) { [weak self] (_) in
            self?.gotoAppPrivacySettings()
        }
        alert.addAction(openSettingsAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func gotoAppPrivacySettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
              UIApplication.shared.canOpenURL(url) else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func showPhotos() {
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        
        galleryImages = [Asset]()
        
        for index in 0..<assets.count {
            let asset = assets.object(at: index)
            galleryImages?.append(Asset(title: "\(index+1)", asset: asset))
        }
        
        applySnapshot(animatingDifferences: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showPhotos":
            let vc = segue.destination as! PageViewController
            vc.currentIndex = selectedIndex!
            vc.galleryImages = galleryImages
        default:
            break
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        customGalleryLayout?.invalidateLayout()
    }
 
    private func makeDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, asset) -> UICollectionViewCell? in
            guard let self = self else { return nil }
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.IDENTIFIER, for: indexPath) as? CollectionViewCell
            
            cell?.setupView(with: asset, size: self.getAvailableSize(for: indexPath))
            
            return cell
        })
      return dataSource
    }
    
    private func getAvailableSize(for indexPath: IndexPath) -> CGSize {
        guard let size = customGalleryLayout?.layoutAttributesForItem(at: indexPath)?.size  else { return CGSize.zero }
        
        return size
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing {
            
        } else {
            selectedIndex = indexPath.row
            performSegue(withIdentifier: "showPhotos", sender: self)
        }
    }
}

extension ViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async { [weak self] in
            let status: PHAuthorizationStatus
            if #available(iOS 14, *) {
                status = PHPhotoLibrary.authorizationStatus(for: .readWrite)
            } else {
                status = PHPhotoLibrary.authorizationStatus()
            }
            self?.showUI(for: status)
        }
    }
}

extension ViewController: CustomGalleryLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, widthAtIndexPath width: CGFloat, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        guard let asset = dataSource.itemIdentifier(for: indexPath) else {
            return CGFloat.zero
        }
        
        let phAsset = asset.asset
        let height = width * CGFloat(phAsset.pixelHeight) / CGFloat(phAsset.pixelWidth)
        
        return height
    }
}

extension ViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        guard let asset = dataSource.itemIdentifier(for: indexPath) else {
            return []
        }
        let itemProvider = NSItemProvider(object: NSString(string: asset.title))
        
        let dragItem = UIDragItem(itemProvider: itemProvider)
        
        dragItem.localObject = asset
        
        return [dragItem]
    }
}

extension ViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        var destinationIndexPath: IndexPath
        
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(row: row - 1, section: 0)
        }
        
        if coordinator.proposal.operation == .move {
            if let item = coordinator.items.first, let sourceIndexPath = item.sourceIndexPath {
                self.galleryImages?.remove(at: sourceIndexPath.row)
                self.galleryImages?.insert(item.dragItem.localObject as! Asset, at: destinationIndexPath.row)
                
                applySnapshot()
                
                coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation: .forbidden)
    }
}
