//
//  BasePageViewController.swift
//  ExerciseImageShuffle
//
//  Created by Rahul Rawat on 02/07/21.
//

import UIKit

class BasePageViewController: UIPageViewController {
    enum ScreenMode {
        case full, normal
    }
    private var _currentMode: ScreenMode!
    private var currentMode: ScreenMode {
        get {
            _currentMode == nil ? defaultMode : _currentMode
        }
        set {
            _currentMode = newValue
        }
    }
    
    var defaultMode: ScreenMode {
        get {
            .normal
        }
    }
    
    var singleTapGestureRecognizer: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didSingleTapWith))
        self.view.addGestureRecognizer(self.singleTapGestureRecognizer)
        changeScreenMode(to: currentMode)
    }
    
    func changeScreenMode(to: ScreenMode) {
        if to == .full {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            UIView.animate(withDuration: 0.25, animations: {
                self.view.backgroundColor = .black
            }, completion: { completed in
            })
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            UIView.animate(withDuration: 0.25, animations: {
                self.view.backgroundColor = .systemBackground
            }, completion: { completed in
            })
        }
        self.currentMode = to
        setNeedsStatusBarAppearanceUpdate()
    }
    
    @objc func didSingleTapWith(_ gestureRecognizer: UITapGestureRecognizer) {
        if self.currentMode == .full {
            changeScreenMode(to: .normal)
        } else {
            changeScreenMode(to: .full)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            self.currentMode == .full
        }
    }
}
