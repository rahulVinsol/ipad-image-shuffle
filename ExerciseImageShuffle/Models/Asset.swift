//
//  Asset.swift
//  ExerciseImageShuffle
//
//  Created by Rahul Rawat on 02/07/21.
//

import Foundation
import Photos

struct Asset: Hashable {
    let title: String
    let asset: PHAsset
}
